#include <iostream>
#include "rc522_.h"

//int main()
//{

//    //rfid::Controller controller;

//    //controller.init();
//    //controller.loop();
//    return 0;
//}


//#include "rc522.h"
//#define RESET_PIN  RPI_V2_GPIO_P1_22

rc522 rc522_;
uint8_t cardType_ = 0;
uint8_t serial_[10];
uint8_t serialLength_ = 0;
uint8_t SAK_[3];
uint8_t buff_[16];

bool isNewCardPresent_()
{
    int status = rc522_.PcdRequest(PICC_REQALL,buff_);
    if (status == TAG_OK) cardType_=(int)(buff_[0]<<8|buff_[1]);
    printf("Status:%u\n", status);
    return (status == TAG_OK || status == TAG_COLLISION);
}

bool readCardSerial_()
{
    if (rc522_.PcdAnticoll(PICC_ANTICOLL1,buff_) != TAG_OK) return false;/* get basic UID from card */
    if (rc522_.PcdSelect(PICC_ANTICOLL1,buff_,SAK_) != TAG_OK) return false;/* select the card */

    if (buff_[0]== 0x88)
    {
        // 0x88 is CT = Cascade Tag (means next level to get UID)
        //copy UID0, UID1, UID2
        memcpy(serial_,&buff_[1],3);

        // 0x95 => get cascade level 2 and select
        if (rc522_.PcdAnticoll(PICC_ANTICOLL2,buff_)!= TAG_OK) return false;
        if (rc522_.PcdSelect(PICC_ANTICOLL2,buff_,SAK_) != TAG_OK) return false;

        if (buff_[0]==0x88)
        {
            // add UID3, UID4 and UID5
            memcpy(serial_+3,&buff_[1],3);

            // 0x97 => get even more extended serial number

            if (rc522_.PcdAnticoll(PICC_ANTICOLL3,buff_)!=TAG_OK)  return false;
            if (rc522_.PcdSelect(PICC_ANTICOLL3,buff_,SAK_)!=TAG_OK) return false;

            // add UID6, UID7, UID8 UID9
            memcpy(serial_+6,buff_,4);
            serialLength_ = 10;        // 10 byte UID (triple size UID) PSS
        }
        else
        {
            // add UID3, UID4, UID5 UID6
            memcpy(serial_+3,buff_,4);
            /* 7 byte UID (double size) Mifare Ultralight, Ultralight C, Desfire
             * Desfire EV1 and Mifare Plus (7 B UID)
             */
            serialLength_ = 7;         // 7 byte Unique ID (double size UID)
        }
    }
    else
    {
        //copy UID0, UID1, UID2, UID3
        memcpy(serial_,&buff_[0],4);
        /* 4 byte non-unique ID (single byte UID) - discontinued end 2010
         * were set at manufacturing.
         * used on Mifare Classic 1K, Mifare Classic 4K and some Mifare Plus versions
         */
        serialLength_ = 4;
    }
    return true;
}

int main()
{
    rc522_.InitRc522();
    while (1)
    {
        if (!isNewCardPresent_())
        {
            usleep(500000);
            continue;
        }
        if (!readCardSerial_())
        {
            printf("Can not read card\n");
            continue;
        }
        printf("Card read ok\n");
        if (serialLength_ == 4)
        {
            printf("Serial: %02x %02x %02x %02x\n", serial_[0],serial_[1],serial_[2],serial_[3]);
        }
        usleep(1000000);
    }
    return 0;
}

