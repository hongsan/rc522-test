import qbs

CppApplication
{
    consoleApplication: true
    files:
    [
        "bcm2835.h",
        //"controller.h",
        "rc522_.cpp",
        "rc522_.h",
        //"MFRC522.h",
        //"MFRC522.cpp",
        "main.cpp",
        //"spi.cpp",
        //"spi.h",
        "PiSPI.h","PiSPI.cpp",

    ]

//    cpp.staticLibraries:
//    [
//        "bcm2835"
//    ]

    Group
    {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
